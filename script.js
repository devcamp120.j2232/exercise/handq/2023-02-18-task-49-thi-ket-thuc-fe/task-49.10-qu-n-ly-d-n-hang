"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "http://203.171.20.210:8080/devcamp-pizza365/orders";

var gOrderList = [];
var gOrderId = "";
var gOrderCode = "";

var gDiscountPercent = 0;

const gTABLE_ROW = ["id", "orderCode", "kichCo", "loaiPizza", "idLoaiNuocUong", "thanhTien", "hoTen", "soDienThoai", "trangThai", "action"];
const gSTT_COL = 0;
const gCODE_COL = 1;
const gCOMBO_COL = 2;
const gPIZZA_COL = 3;
const gDRINK_COL = 4;
const gTOTAL_COL = 5;
const gNAME_COL = 6;
const gPHONE_COL = 7;
const gSTATUS_COL = 8;
const gACTION_COL = 9;

var gStt = 1;
var gTableOrder = $("#order-table").DataTable({
    columns: [
        {data: gTABLE_ROW[gSTT_COL]},
        {data: gTABLE_ROW[gCODE_COL]},
        {data: gTABLE_ROW[gCOMBO_COL]},
        {data: gTABLE_ROW[gPIZZA_COL]},
        {data: gTABLE_ROW[gDRINK_COL]},
        {data: gTABLE_ROW[gTOTAL_COL]},
        {data: gTABLE_ROW[gNAME_COL]},
        {data: gTABLE_ROW[gPHONE_COL]},
        {data: gTABLE_ROW[gSTATUS_COL]},
        {data: gTABLE_ROW[gACTION_COL]},
    ],
    columnDefs: [
        {
            targets: gSTT_COL,
            render: function() {
                return gStt++
            }
        },
        {
            targets: gNAME_COL,
            className: "text-nowrap",
        },
        {
            targets: gACTION_COL,
            className: "text-nowrap",
            defaultContent: 
            `
                <button id="btn-detail" class="btn btn-primary"><i class="fa-solid fa-circle-info"></i> &nbsp; Detail</button>
                <button id="btn-delete" class="btn btn-danger"><i class="fa-solid fa-trash-can"></i> &nbsp; Delete</button>
            `
        },
    ]
})

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
// gán sự kiện tải trang/F5
$(document).ready(function() {
    onPageLoading();
});

// gán sự kiện click button Filter
$(document).on("click", "#btn-filter", onBtnFilterClick);

// gán sự kiện click button New Order
$(document).on("click", "#btn-create", onBtnCreateOrderClick);
// gán sự kiện click button Create Order trên Modal Create Order
$(document).on("click", "#btn-create-order", onBtnCreateOrderOnModalClick);

// gán sự kiện on change cho select Combo trên form Modal Create Order
$(document).on("change", "#create-kich-co", function() {
    loadComboDetail(this.value);
})
// gán sự kiện on change cho input Voucher trên form Modal Crate Order
$(document).on("change", "#create-voucher", function() {
    getDiscoutValueByVoucherIdChange(this.value);
})
// gán sự kiện on change cho input Thành tiền trên form Modal Crate Order
$(document).on("change", "#create-thanh-tien", function() {
    getDiscountValueByPriceChange(this.value);
})

// gán sự kiện click button Detail
$(document).on("click", "#btn-detail", onBtnDetailClick);
// gán sự kiện click button Update Order trên Modal Order Detail
$(document).on("click", "#btn-update-order", onBtnUpdateOrderOnModalClick);

// gán sự kiện click button Delete
$(document).on("click", "#btn-delete", onBtnDeleteClick);
// gán sự kiện click button Xóa đơn hàng trên Modal Delete Order
$(document).on("click", "#btn-delete-order", onBtnDeleteOrderOnModalClick);

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// function xử lý sự kiện load trang/F5
function onPageLoading() {
    // Gọi API lấy danh sách đơn hàng và lưu vào biến toàn cục gOrderList
    apiGetOrderList();
    // Gọi API lấy danh sách nước uống
    let vAjaxRequest = apiGetDrinkList();
    vAjaxRequest.done(function(drinkList) {
        // Load drink list vào select trên form Modal Create Order
        loadDrinkListToSelect(drinkList);
    })
    // Hiển thị table data 
    insertOrderListToTable(gOrderList);
}

// function xử lý sự kiện click button Filter
function onBtnFilterClick() {
    // Khai báo đối tượng chứa dữ liệu filter
    let vFilterData = {
        trangThai: "",
        loaiPizza: "",
    };
    // Thu thập dữ liệu
    getFilterData(vFilterData);
    console.log(vFilterData);
    // Kiểm tra dữ liệu
    // Xử lý hiển thị
    displayFilteredOrderData(vFilterData);
}

// function xử lý sự kiện click button New Order
function onBtnCreateOrderClick() {
    $("#modal-order-create").modal("show");
}

// function xử lý sự kiện click button Create Order trên Modal Create Order
function onBtnCreateOrderOnModalClick() {
    // Khai báo đối tượng chứa dữ liệu
    let vNewOrderData = {
        kichCo: "",
        duongKinh: "",
        suon: "",
        salad: "",
        loaiPizza: "",
        idVourcher: "",
        idLoaiNuocUong: "",
        soLuongNuoc: "",
        hoTen: "",
        thanhTien: "",
        email: "",
        soDienThoai: "",
        diaChi: "",
        loiNhan: ""
    };
    // B1: Thu thập dữ liệu
    getNewOrderData(vNewOrderData);
    // B2: Kiểm tra dữ liệu
    let vDataValid = checkOrderData(vNewOrderData);
    if (vDataValid) {
        // ẩn modal
        $("#modal-order-create").modal("hide");
        // B3: Gọi API tạo đơn hàng mới
        let vAjaxRequest = apiCreateNewOrder(vNewOrderData);
        vAjaxRequest.done(function() {
            alert("Tạo đơn hàng mới thành công");
            // xóa dữ liệu điền trên form
            clearModalCreateOrderForm();
            // Tải lại order list và tải lại table data
            apiGetOrderList();
            insertOrderListToTable(gOrderList);
        }).fail(function() {
            alert("Tạo đơn hàng mới không thành công");
        })
    }
}

// function xử lý sự kiện click button Detail (Chi tiết)
function onBtnDetailClick() {
    // hiện modal Order Detail
    $("#modal-order-detail").modal("show");
    // thu thập dữ liệu ID và Order code lưu vào biến toàn cục
    getUserDetailOnClick(this);
    // gọi API lấy thông tin đơn hàng bằng order code
    let vAjaxRequest = apiGetOrderDetailByOrderCode();
    vAjaxRequest.done(function(orderDetail) {
        // Xử lý hiển thị dữ liệu
        displayOrderDetail(orderDetail);
    });
}

// function xử lý sự kiện click button Update Order trên Modal Order Detail
function onBtnUpdateOrderOnModalClick() {
    // Khai báo đối tượng chứa dữ liệu
    let vOrderStatus = {
        trangThai: ""
    };
    // B1: Thu thập dữ liệu
    getOrderStatus(vOrderStatus);
    // B2: Kiểm tra dữ liệu
    let vDataValid = checkOrderStatus(vOrderStatus);
    if (vDataValid) {
        // ẩn modal
        $("#modal-order-detail").modal("hide");
        // B3: Gọi API update trạng thái đơn hàng
        let vAjaxRequest = apiUpdateOrder(vOrderStatus);
        vAjaxRequest.done(function() {
            alert("Update trạng thái đơn hàng thành công");
            // Tải lại order list và tải lại table data
            apiGetOrderList();
            insertOrderListToTable(gOrderList);
        }).fail(function() {
            alert("Update trạng thái đơn hàng thất bại");
        })
    }
}

// function xử lý sự kiện click button Delete
function onBtnDeleteClick() {
    // hiện modal Order Detail
    $("#modal-order-delete").modal("show");
    // thu thập dữ liệu ID và Order code lưu vào biến toàn cục
    getUserDetailOnClick(this);
}

// function xử lý sự kiện click button Delete Order trên Modal Delete Order
function onBtnDeleteOrderOnModalClick() {
    // B1: Thu thập dữ liệu
    // B2: Kiểm tra dữ liệu
    // ẩn modal
    $("#modal-order-delete").modal("hide");
    // B3: Gọi API xóa đơn hàng theo order ID
    let vAjaxRequest = apiDeleteOrderByOrderId();
    vAjaxRequest.done(function() {
        alert("Xóa đơn hàng thành công");
        // Tải lại order list và tải lại table data
        apiGetOrderList();
        insertOrderListToTable(gOrderList);
    }).fail(function() {
        alert("Xóa đơn hàng thất bại");
    })
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

//------//
// API  //
//------//

// function gọi API lấy danh sách drink
function apiGetDrinkList() {
    return $.ajax({
        url: "http://203.171.20.210:8080/devcamp-pizza365/drinks",
        type: "GET",
    });
}

// function gọi API lấy thông tin đơn hàng bằng order code
function apiGetOrderDetailByOrderCode() {
    return $.ajax({
        url: gBASE_URL + "/" + gOrderCode,
        type: "GET",
    });
}

// function tìm phần trăm giảm giá theo Voucher ID
// functiont trả lại số % được giảm giá; return 0 nếu không tìm thấy voucher
function apiGetDiscoutPercent(paramVoucherId) {
    // Trường hợp user điền mã xong xóa thì trả về phần trăm giảm giá = 0
    if ($("#create-voucher").val() === "") {
        gDiscountPercent = 0;
    }
    else {
        $.ajax({
            url: "http://203.171.20.210:8080/devcamp-pizza365/voucher_detail/" + paramVoucherId,
            type: "GET",
            async: false,
        }).done(function(discount) {
            // Nếu lấy % giảm giá về là số âm, hoặc số > 100 => sai
            if (discount < 0 || discount > 100) {
                gDiscountPercent = 0;
                alert("Mã giảm giá không tồn tại");
            }
            else {
                gDiscountPercent = discount.phanTramGiamGia;
                alert("Voucher giảm giá " + gDiscountPercent + "%");
            }
        }).fail(function() {
            gDiscountPercent = 0;
            alert("Mã giảm giá không tồn tại");
        })
    }
}

// function gọi API lấy danh sách đơn hàng và lưu vào biến toàn cục gOrderList 
function apiGetOrderList() {
    return $.ajax({
        url: gBASE_URL,
        type: "GET",
        async: false,
    }).done(function(orderList) {
        gOrderList = orderList;
    }).fail(function() {
        console.log("Lấy dữ liệu Order List không thành công");
    });
}

// function gọi API để cập nhật trạng thái confirm hoặc cancel cho 01 order (update)
function apiUpdateOrder(paramStatus) {
    return $.ajax({
        url: gBASE_URL + "/" + gOrderId,
        type: "PUT",
        data: JSON.stringify(paramStatus),
        contentType: "application/json",
    });
}

// function gọi API tạo đơn hàng mới
function apiCreateNewOrder(paramOrderData) {
    return $.ajax({
        url: gBASE_URL,
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(paramOrderData)
    });
}

// function gọi API để cập nhật trạng thái order (update)
function apiUpdateOrder(paramStatus) {
    return $.ajax({
        url: gBASE_URL + "/" + gOrderId,
        type: "PUT",
        data: JSON.stringify(paramStatus),
        contentType: "application/json",
    });
}

// function gọi API xóa đơn hàng theo order ID
function apiDeleteOrderByOrderId() {
    return $.ajax({
        url: gBASE_URL + "/" + gOrderId,
        type: "DELETE",
    });
}

//-----------//
// Load data //
//-----------//

// function insert danh sách đơn hàng vào table
function insertOrderListToTable(paramOrderList) {
    gStt = 1;
    gTableOrder.clear();
    gTableOrder.rows.add(paramOrderList);
    gTableOrder.draw();
}

// function xử lý filter data theo firstname và lastname
function displayFilteredOrderData(paramFilterData) {
    let vFilteredData = gOrderList.filter(function(order) {
        return (order.trangThai.toLowerCase() == paramFilterData.trangThai || paramFilterData.trangThai == "") 
            && (order.loaiPizza.toLowerCase() == paramFilterData.loaiPizza || paramFilterData.loaiPizza == "");
    })
    insertOrderListToTable(vFilteredData);
}

// function load drink list vào select
function loadDrinkListToSelect(paramDrinkList) {
    for (let drink of paramDrinkList) {
        $("#create-drink").append(
            $("<option>").val(drink.maNuocUong).text(drink.tenNuocUong)
        );
    }
}

// function get combo detail theo kích cỡ được chọn
function getCombo(paramDuongKinh, paramSuon, paramSalad, paramDrink, paramPrice) {
    $("#create-duong-kinh").val(paramDuongKinh);
    $("#create-suon").val(paramSuon);
    $("#create-salad").val(paramSalad);
    $("#create-drink-number").val(paramDrink);
    $("#create-thanh-tien").val(paramPrice).trigger('change');
}

// function load dữ liệu order theo select kích cỡ lên form Modal Create Order
function loadComboDetail(paramCombo) {
    // Gán giá trị tương ứng theo kích cỡ cho các mục input
    if (paramCombo == "S") {
        getCombo(20, 2, 200, 2, 150000);  
    }
    else if (paramCombo == "M") {
        getCombo(25, 4, 300, 3, 200000);   
    }
    else if (paramCombo == "L") {
        getCombo(30, 8, 500, 4, 250000);
    }
    else if (paramCombo == "") {
        getCombo("", "", "", "", "");
    }
}

// function kiểm tra và load dữ liệu Giảm giá theo Voucher ID lên form Modal Create Order
// input: Voucher ID
// output: giá trị Giảm giá
function getDiscoutValueByVoucherIdChange(paramVoucherId) {
    // trường hợp chưa chọn combo (chưa có dữ liệu đơn giá)
    if ($("#create-kich-co").val() == "") {
        alert("Vui lòng chọn Combo Pizza trước khi nhập voucher");
        // xóa dữ liệu trong input voucher id
        $("#create-voucher").val("");
    }
    else {
        // Gọi API lấy phần trăm giảm giá theo voucher id
        apiGetDiscoutPercent(paramVoucherId);
        let vComboPrice = parseInt($("#create-thanh-tien").val());
        let vDiscountPrice = vComboPrice * (gDiscountPercent / 100);
        // hiển thị giá trị giảm giá lên form
        $("#create-discount").val(vDiscountPrice);
    }
}

// function kiểm tra và load dữ liệu Giảm giá theo Thành Tiền lên form Modal Create Order
// input: giá trị Thành tiền
// output: giá trị Giảm giá
function getDiscountValueByPriceChange(paramPrice) {
    let vDiscountPrice = paramPrice * (gDiscountPercent / 100);
    // hiển thị giá trị giảm giá lên form
    $("#create-discount").val(vDiscountPrice);
}

//--------------//
// Collect Data //
//--------------//

// function thu thập dữ liệu filter
// function return tham số đối tượng paramFilterData chứa dữ liệu người dùng chọn trên form
function getFilterData(paramFilterData) {
    paramFilterData.trangThai = $("#select-status").val();
    paramFilterData.loaiPizza = $("#select-pizza").val();
}

// function thu thập dữ liệu của Row khi click button Detail
function getUserDetailOnClick(paramButton) {
    let vRowClicked = $(paramButton).closest("tr");
    let vUserData = gTableOrder.row(vRowClicked).data();
    gOrderCode = vUserData.orderCode;
    gOrderId = vUserData.id;
}

// function thu thập dữ liệu new order trên form Modal Create Order
// function trả về đối tượng paramOrderData được tham số hóa
function getNewOrderData(paramOrderData) {
    paramOrderData.kichCo = $("#create-kich-co").val();
    paramOrderData.duongKinh = $("#create-duong-kinh").val();
    paramOrderData.suon = $("#create-suon").val();
    paramOrderData.salad = $("#create-salad").val();
    paramOrderData.loaiPizza = $("#create-loai-pizza").val();
    paramOrderData.idVourcher = $("#create-voucher").val().trim();
    paramOrderData.idLoaiNuocUong = $("#create-drink").val();
    paramOrderData.soLuongNuoc = $("#create-drink-number").val();
    paramOrderData.hoTen = $("#create-fullname").val().trim();
    paramOrderData.thanhTien = $("#create-thanh-tien").val();
    paramOrderData.email = $("#create-email").val().trim();
    paramOrderData.soDienThoai = $("#create-phone").val().trim();
    paramOrderData.diaChi = $("#create-address").val().trim();
    paramOrderData.loiNhan = $("#create-message").val().trim();
}

// function thu thập dữ liệu order status trên form Modal Order Detail
// function trả về đối tượng paramOrderStatus được tham số hóa
function getOrderStatus(paramOrderStatus) {
    paramOrderStatus.trangThai = $("#input-status").val();
}

//------------//
// Check Data //
//------------//

// function kiểm tra email
// function return true nếu tất cả thông tin hợp lệ. Return false nếu có thông tin không hợp lệ
function checkEmail(paramOrderData) {
    let vCharBefore = paramOrderData.email.split("@")[0];
    let vCharAfter = paramOrderData.email.split("@")[1];
    if (paramOrderData.email != "") {
        if (!paramOrderData.email.includes("@")) {
            alert("Email phải có @");
            return false;
        }
        else if (vCharBefore == "" || vCharAfter == "") {
            alert("Email phải có kí tự trước và sau @");
            return false;
        }
    }
    return true;
}

// function kiểm tra dữ liệu trên form
// function return true nếu tất cả thông tin hợp lệ. Return false nếu có thông tin không hợp lệ
function checkOrderData(paramOrderData) {
    // kiểm tra menu
    if (paramOrderData.kichCo == "") {
        alert("Vui lòng chọn Menu Combo");
        return false;
    }
    // kiểm tra loại pizza
    else if (paramOrderData.loaiPizza == "") {
        alert("Vui lòng chọn Loại Pizza");
        return false;
    }
    else if (paramOrderData.idLoaiNuocUong == "") {
        alert("Vui lòng chọn thức uống");
        return false;
    }
    else if (paramOrderData.hoTen == "") {
        alert("Vui lòng nhập họ và tên");
        return false;
    }
    else if (!checkEmail(paramOrderData)) {
        return false;
    }
    else if (paramOrderData.soDienThoai == "") {
        alert("Vui lòng nhập số điện thoại");
        return false;
    }
    else if (paramOrderData.diaChi == "") {
        alert("Vui lòng nhập địa chỉ");
        return false;
    }
    return true;
}

// function kiểm tra dữ liệu Order status
// function return true nếu tất cả thông tin hợp lệ. Return false nếu có thông tin không hợp lệ
function checkOrderStatus(paramOrderStatus) {
    if (paramOrderStatus.trangThai == "") {
        alert("Vui lòng chọn trạng thái order cần cập nhật");
        return false;
    }
    return true;
}

//--------------//
// Display Data //
//--------------//

// function hiển thị dữ liệu order detail lên Modal Order Detail
function displayOrderDetail(paramOrderDetail) {
    $("#input-ma-don").val(paramOrderDetail.orderCode);
    $("#input-kich-co").val(paramOrderDetail.kichCo);
    $("#input-duong-kinh").val(paramOrderDetail.duongKinh);
    $("#input-suon").val(paramOrderDetail.suon);
    $("#input-salad").val(paramOrderDetail.salad);
    $("#input-loai-pizza").val(paramOrderDetail.loaiPizza);
    $("#input-voucher").val(paramOrderDetail.idVourcher);
    $("#input-thanh-tien").val(paramOrderDetail.thanhTien);
    $("#input-giam-gia").val(paramOrderDetail.giamGia);
    $("#input-drink").val(paramOrderDetail.idLoaiNuocUong);
    $("#input-drink-number").val(paramOrderDetail.soLuongNuoc);
    $("#input-fullname").val(paramOrderDetail.hoTen);
    $("#input-email").val(paramOrderDetail.email);
    $("#input-phone").val(paramOrderDetail.soDienThoai);
    $("#input-address").val(paramOrderDetail.diaChi);
    $("#input-message").val(paramOrderDetail.loiNhan);
    $("#input-status").val(paramOrderDetail.trangThai);
    $("#input-day-create").val(paramOrderDetail.ngayTao);
    $("#input-day-modify").val(paramOrderDetail.ngayCapNhat);
}

// function clear form Modal Create Order
function clearModalCreateOrderForm() {
    $("#create-kich-co").val("");
    $("#create-duong-kinh").val("");
    $("#create-suon").val("");
    $("#create-salad").val("");
    $("#create-loai-pizza").val("");
    $("#create-voucher").val("");
    $("#create-discount").val("0");
    $("#create-drink").val("");
    $("#create-drink-number").val("");
    $("#create-fullname").val("");
    $("#create-thanh-tien").val("");
    $("#create-email").val("");
    $("#create-phone").val("");
    $("#create-address").val("");
    $("#create-message").val("");
}